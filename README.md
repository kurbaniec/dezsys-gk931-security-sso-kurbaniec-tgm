# GK9.3 Middleware Engineering "Security Concepts" DEZSYS_GK931_SECURITY_SSO
# Einführung

Diese Uebung soll helfen die Funktionsweise eines Central Authentication Service (CAS). Dabei sollen erste Erfahrung mit den Technologien Single Sign On (SSO) und Ticket Granting Ticket Service (TGT) gemacht werden.

## 1.1 Ziele

Das Ziel dieser Uebung ist die Installation eines CAS-Servers und das Testen einer Anbindung mittels einem CAS-Clients. Die CAS Implementierung basiert auf einem CAS Protokoll, das mit Hilfe von 3 Teilen umgesetzt wird: der CAS-Client (meist ein Webbrowser) stellt eine Anfrage an ein Webapplikation stellt. Die Nutzung der Webapplikation erfordert einen Authentifikation, aus diesem Grund wird der Client an den CAS-Server weitergleitet. Wenn dieser Schritt erfolgreich war, dann wird vom CAS-Server ein Service-Ticket an den Client ausgestellt. Der Client wiederrum leitet das Ticket an die Webapplikation weiter, wo eine sichere Verbindung zwischen Webapplikation und CAS-Server aufgebaut wird, um das Ticket zu prüfen. Wenn diese Validierung erfolgreich war, kann das Resultat der Webapplikation an den Client zurückgegeben werden.

Die CAS-Architektur sieht folgendermassen aus:  
![CAS Architecture Diagram](cas_architecture.png)

## 1.2 Voraussetzungen

Grundlagen zu Central Authentication Service, Single Sign On- und Ticket Granting Ticket Service
Java Programmierkenntnisse
Installation Java JDK 1.8, Apache Tomcat
Installation und Verwendung von Repository Management git
Verwendung von Build Tools maven oder gradle


## 1.3 Aufgabenstellung

* Installieren Sie eine CAS-Server und probieren Sie damit eine Webapplikation Ihrer Wahl einzubinden.
* Download: Apereo CAS 5.0.9 [Link](https://github.com/apereo/cas/releases)
* Installation einer Demo Anwendung
* Entwicklung einer eigenen Webapplikation, die in der CAS Infrastruktur eingebunden wird
* Entwerfen Sie ein Konzept zum Testen der SSO- und TGT-Funktionalität
* Beschreiben Sie 3 Testfaelle fuer einen CAS-Server?

## 1.4 Bewertung

﻿Gruppengrösse: 1 Person  
* Anforderungen "überwiegend erfüllt"  
 * Installation Apereo CAS Servers  
 * Konfiguration des CAS Servers  
 * Dokumentation der Installations- und Konfigurationsschritte  
 * Installation einer Demo Webapplikation  
 * Dokumentation der Installationsschritte  
 * Testen der Demo Webapplikation und dokumentieren der einzelnen Testfälle und deren Ergebnisse  
* Anforderungen "zur Gänze erfüllt"  
 * Implementieren & Dokumentieren einer eigenen Webapplikation und deren Anbindung an den CAS-Server
 * Konzeptionierung um die SSO- und TGT-Funktionalität zu testen
 * Durchführen & Dokumentieren der Testszenarien


## 1.5 Fragestellung für Protokoll

* Was bedeutet Single Sign On?
* Zeigen Sie in einem Diagramm die Ablaeufe von CAS Client, CAS Server, Service und User.
* Was ist ein TGT und beschreiben Sie die Funktionsweise eine TGT?
* Welche Features/Merkmale bietet der Apereo CAS Server an?
* Beschreiben Sie den CAS Server einer CAS Architektur?
* Beschreiben Sie den CAS Client einer CAS Architektur?
* Was ist die Aufgabe eines Tickets in einem TGT Service?


## 1.6 Weiterführende Dokumentation & Links

 * [Kerberos Documentation](http://www.kerberos.org/software/tutorial.html)  
 * [CAS - Central Authentication Service (Introduction)](http://robert-rusu.blogspot.co.at/2015/03/cas-central-authentication-service.html)  
 * https://apereo.github.io/cas/developer/Build-Process.html  
 * https://apereo.github.io/cas/5.1.x/index.html  
 * https://apereo.github.io/cas/5.1.x/planning/Installation-Requirements.html  
 * https://apereo.github.io/cas/5.1.x/installation/Configuration-Management.html  

# Implementierung

## Installation:

Der ganze Source-Code kann im Repository gefunden werden.

### Vorrausetzungen:

* [JDK 11]( https://github.com/ojdkbuild/ojdkbuild )
* CAS 6.2.x

### CAS-Server

Für den CAS-Server wird ein vorgefertigtes Template verwendet. Zuerst holen wir uns das Template:

```
git clone https://github.com/apereo/cas-overlay-template.git cas-server
```

In der `build.gradle` muss folgende Dependency für den CAS-Server entkommentiert werden:

```groovy
dependencies {
    // Other CAS dependencies/modules may be listed here...
    compile "org.apereo.cas:cas-server-support-json-service-registry:${casServerVersion}"
}
```

Ich persönlich habe im Path JDK8, das Projekt erfordert aber JDK11. Man kann aber Gradle sagen, dass er das JDK11 benutzen soll. Dazu wird das `gradle.properties`-File wie folgt geändert:

```groovy
# Use JDK11
org.gradle.java.home=C:\\Program Files\\Java\\jdk11.0.5-1
```

Jetzt kann das Programm gebuilded werden:

```
gradlew build
```

Als nächstes muss der Ordner `cas-server/src/main/resources` erstellt werden.  In diesem wird der Ordner `cas-server/etc` reinkopiert. Jetzt muss das `application.properties`-File unter  `server/src/main/resources/` erstellt werden. In diesem muss die SSL-Konfiguration angepasst werden, dabei wird auch der Server Port verändert:

```
server.ssl.key-store=classpath:/etc/cas/thekeystore
server.ssl.key-password=changeit
standalone.config=classpath:/etc/cas/config
server.port=6443
```

Als nächstes wird ein SSL Key Store generiert. Dazu muss man sich im `cas-server/src/main/resources/etc/cas ` Ordner befinden und folgenden Befehl ausführen:

`Hinweis`: Falls man Java 11 im Path hat kann man einfach `keytool` als Befehl verwenden anstatt dem genauen Pfad

```
C:\"Program Files"\Java\jdk11.0.5-1\bin\keytool.exe -genkey -keyalg RSA -alias thekeystore -keystore thekeystore -storepass changeit -validity 360 -keysize 2048
```

Nach der Ausführung kommen mehre Fragen. Das Passwort ist `changeit`, alle Angaben müssen mit `localhost` beantwortet werden.

Als nächstes muss das Keyfile in das `.crt` Format exportiert werden.

```
C:\"Program Files"\Java\jdk11.0.5-1\bin\keytool.exe -export -alias thekeystore -file thekeystore.crt -keystore "C:\Arbeit\SYT\5xHIT\WS\dezsys-gk931-security-sso-kurbaniec-tgm\cas-server\src\main\resources\etc\cas\thekeystore"
```

Dieses wird dann in den Java cacerts Keystore exportiert.

```
C:\"Program Files"\Java\jdk11.0.5-1\bin\keytool.exe -import -alias thekeystore -storepass changeit -file "C:\Arbeit\SYT\5xHIT\WS\dezsys-gk931-security-sso-kurbaniec-tgm\cas-server\src\main\resources\etc\cas\thekeystore.crt"
 -keystore "C:\Program Files\Java\jdk11.0.5-1\lib\security\cacerts"
```

```
keytool -import -alias thekeystore -storepass changeit -file "C:\Arbeit\SYT\5xHIT\WS\dezsys-gk931-security-sso-kurbaniec-tgm\cas-server\src\main\resources\etc\cas\thekeystore.crt" -keystore "C:\Program Files\Java\jdk1.8.0_211\jre\lib\security\cacerts"
```

Jetzt sollte der Server startbar sein

```
gradlew run
```

Jedoch gibt CAS einen Fehler zurück, dass er die Konfiguration-Dateien nicht finden kann. Wie es sich herausgestellt hat, sucht er diese unter `C:/etc/cas` anstatt im Arbeitsorder. Die Lösung für das Problem war das manuelle Angeben der Konfigurationsdatei mittels `cas.standalone.configurationDirectory` im `application.properties`-File. 

```
# Spring config
server.port=6443
spring.main.allow-bean-definition-overriding=true
# SSL config
server.ssl.key-store=${user.dir}/src/main/resources/etc/cas/thekeystore
server.ssl.key-password=changeit
# CAS config
## Overall
cas.standalone.configurationDirectory=file:${user.dir}/src/main/resources/etc/cas/config
cas.standalone.configurationFile=file:${user.dir}/src/main/resources/etc/cas/config/cas.properties
cas.server.prefix=${cas.server.name}/cas
## Add user to CAS
cas.authn.accept.users=casuser::Mellon
```

Weiters aktiviert die Konfiguration die notwendige Bean-Überschreibung und gibt einen Standarduser `casuser` mit dem Password `Mellon` zur CAS Instanz hinzu. 

Jetzt sollte das Starten per `gradlew run` funktionieren.

Jetzt geht man auf  https://localhost:6443/ und kann sich mit dem Standarduser anmelden.

![CAS Login](images/cas-login.PNG)

![CAS Login](images/cas-login-successful.PNG)

Der nächste Schritt ist die Erstellung einer Konfiguration, um die Registrierung von Client-Servern zu erlauben. Dazu kann JSON Konfiguration verwendet werden. Zuerst muss aber die Service Registrierung per JSON in der `application.properties ` des CAS-Servers eingeschal:

```
## Service Registry
cas.serviceRegistry.initFromJson=true
cas.serviceRegistry.json.location=file:${user.dir}/src/main/resources/etc/cas/services
```

Achtung! Unbedingt `file:`  am Anfang verwenden, sonst funktioniert es nicht!

Als nächstes kann die Konfiguration für unseren CAS-Client erstellt werden, der die CAS Authentifizierung nutzen möchte. Diese wird im Ordner, der in der `application.properties` eingetragen wurde erstellt, z.B.`src/main/resouces/etc/cas/services/casSecuredApp-19991.json`:

```json
{
    "@class" : "org.apereo.cas.services.RegexRegisteredService",
    "serviceId" : "^http://localhost:9000/login/cas",
    "name" : "CAS Spring Secured App",
    "description": "This is a Spring App that usses the CAS Server for it's authentication",
    "id" : 19991,
    "evaluationOrder" : 1
}
```

### CAS-Client

Die Client-Applikation, die den CAS-Server testen sollte, ist eine einfache Spring-Boot Applikation die für Spring-Security den CAS-Server nutzt, um gesicherte Seiten damit zu schützen.

Der generelle Vorgang ist dieser:

* Ein User will eine gesicherten Website am Client-Server aufrufen
* Dadurch wird eine sogenannter `ÀuthenticationEntryPoint` getriggered. Dieser wird im Programmcode als Bean definiert, die die URL von der Login-Seite des CAS-Servers enthält. Beim Triggern von diesem, wird der User an diese URL zur Anmeldung weitergeleitet.
* Bei einer erfolgreichen Authentifizierung, wird der User  zurück zum Client-Server geleitet, wobei das Service Ticket als Query Parameter in der URL hinzugefügt wird.
* Als nächstes wird der `CasAuthenticationFilter` ausgeführt, der Überprüft das Ticket intern auf Gültigkeit.
* Falls das Ticket valide ist, wird der User zur der Seite weitergeleitet, die er ursprünglich aufrufen wollte.

Dies bedeutet in der Praxis, dass wenn man sich am Client-Server anmeldet (`localhost:9000/login`), wird man auf die CAS-Applikation umgeleitet (`localhost:6443/login`) um dort die Anmeldedaten einzugeben. Bei Verwendung von falschen Anmeldedaten, meldet CAS dies:

![](images/client-login-failure.PNG)

Bei erfolgreichen Login sollte zurück auf die Client-Applikation weitergeleitet werden (`localhost:9000/login/cas`). Jedoch funktioniert dies bei mir nicht.

Anfangs hatte ich folgende Fehlermeldung bei der Rückleitung:

`java.security.cert.CertificateException: No subject alternative DNS name matching localhost found.`

Dieser war verständlich, da ich für meinen Client-Server eine andere JDK verwendet habe, die das Zertifikat (was in der JDK11 eingetragen ist) nicht vorhanden hatte. 

Ich wechselte beim Client die JDK auf die JDK11, die der CAS-Server auch verwendet und das Zertifikat enthält.

Doch dies führte zu einem neuen Fehler:![](images/client-login.PNG)

Den Grund dafür konnte ich in den Apereo Troubleshooting Guide finden:

"PKIX path building errors are the most common SSL errors. The problem here is that the CAS client does not trust the certificate presented by the CAS server; most often this occurs because of using a *self-signed certificate* on the CAS server. To resolve this error, import the CAS server certificate into the system truststore of the CAS client. If the certificate is issued by your own PKI, it is better to import the root certificate of your PKI into the CAS client truststore." 

In kurz, das Zertifikat wird nicht erkannt. Dies ist nicht nachvollziehbar, da der Client und Server auf der gleichen JDK laufen und somit Zugriff auf das selbe Zertifikat haben.

Zum Testen, ob das Zertifikat vorhanden ist, habe ich folgenden Befehl verwendet:

![](images/certificate.PNG)

Das Zertifikat für `localhost` ist vorhanden.

Ich hab mehrere Wege probiert um den Fehler zu lösen:

* Beim Start der Client-Applikation der JVM sagen, wo das Zertifikat zu finden ist. Dazu werden folgende Argumente hinzugefügt:

  ```
  -Djavax.net.ssl.trustStorePassword=changeit -Djavax.net.ssl.trustStore="C:\Program Files\Java\jdk11.0.5-1\lib\security\cacerts"
  ```

* Den Tomcat-Server der Client-Applikation auf HTTPS anstatt HTTP umstellen und mit dem selben Zertifikat wie der CAS-Server betreiben:

  `application.properties`:

  ```
  server.ssl.key-store=file:${user.dir}/src/main/resources/keystore/thekeystore
  server.ssl.key-password=changeit
  ```

All diese Lösungsansätze haben nicht funktioniert und konnten das Problem nicht lösen. Ich befürchte, dass vielleicht die Adresse `localhost` Probleme bereitet und vielleicht bei der JDK11 aus Sicherheitsgründen für Zertifikate nicht erlaubt ist.

## Fragestellungen

* Was bedeutet Single Sign On?

  Single Sign On ermöglicht es sich auf mehreren zugehörigen, jedoch komplett unterschiedlichen Services durch ein einmaliges Login zu authentifizieren. Das Login wird zentral durchgeführt (z.B. durch die Weiterleitung an einen CAS Server) durch die der User die Erlaubnis bekommt, alle zugehörigen Dienste zu benutzen. Beispiel, wenn man sich mit einem Google-Account bei Gmail anmeldetet, wird diese Anmeldung bei anderen Diensten (Docs, Youtube) weiter verwendet.

* Zeigen Sie in einem Diagramm die Ablaeufe von CAS Client, CAS Server, Service und User.

  Das nachfolgende Ablaufdiagramm, zeigt wie ein User sich bei zwei Services (`Protected App` und `Protected App#2`) anmelden will, die über einen CAS-Server gesichert werden. 

  Beim Anzeigen des ersten gesicherten Services wird der User zum CAS-Server weiteregeleitet, um sich dort per Login anzumelden. Bei Erfolgreicher Anmeldung bekommt der User ein Ticket Granting Ticket (TGT) und wird zurück zum Service weiter geleitet. Dieser bekommt ein durch das TGT generiertes Ticket, das Service Ticket, zurück und überprüft dieses Ticket auf Gültigkeit und zeigt die Seite an, wenn dies der Fall ist.

  Beim zweiten Service kommt es zu einem ähnlichen Ablauf, jedoch wird der User nicht nach einem Login beim CAS-Server gefragt, da er ja schon das TGT als Cookie besitzt. Dieses wird validiert, ein neues Service Ticket wird erstellt und bei Erfolg wird man zum zweiten Service weitergeleitet.

  ![](images/cas-ablauf.PNG)

  [Quelle]( https://apereo.github.io/cas/4.2.x/protocol/CAS-Protocol.html#planning )

* Was ist ein TGT und beschreiben Sie die Funktionsweise eine TGT?

  Das Ticket Granting Ticket (TGT) repräsentiert eine Single-Sign-On Session, die beim User als Cookie abgespeichert wird. Dieses wird vom CAS-Server ausgestellt und ermöglich es einen Benutzer über mehre Services, die mit der CAS Authentifizierung arbeiten, zu identifizieren und mehrmaliges Login zu unterbinden. Das TGT wird außerdem benötigt um sogenannte Service Tickets (ST) zu generieren. Diese werden benötigt um den User innerhalb eines individuellen CAS-gestützten Services zu identifizieren. 

* Welche Features/Merkmale bietet der Apereo CAS Server an?

  Apereo bewirbt das Projekt auf seiner [Seite](https://www.apereo.org/projects/cas) mit folgenden Worten:

  CAS provides enterprise single sign-on service for the Web:

  - An open and well-documented protocol
  - An open-source Java server component
  - Pluggable authentication support (LDAP, database, X.509, 2-factor)
  - Support for multiple protocols (CAS, SAML, OAuth, OpenID)
  - A library of clients for Java, .Net, PHP, Perl, Apache, [uPortal](https://www.apereo.org/projects/uportal/uportal-community), and others
  - Integrates with [uPortal](https://www.apereo.org/projects/uportal/uportal-community), BlueSocket, TikiWiki, Mule, Liferay, Moodle and others
  - Community documentation and implementation support
  - An extensive community of adopters

* Beschreiben Sie den CAS Server einer CAS Architektur?

  Der CAS-Server ist in erster Linie für die Authentifizierung von Benutzern und die Gewährung des Zugriffs auf CAS-fähige Services, den CAS-Clients, durch die Ausstellung und Validierung von Tickets zuständig ist. Eine Single-Sign-On Session wird erstellt, wenn der Server dem Benutzer bei erfolgreicher Anmeldung ein Ticket-erteilendes Ticket (TGT) ausstellt. 

* Beschreiben Sie den CAS Client einer CAS Architektur?

  Der Begriff des CAS-Clients kann auf zwei Arten interpretiert werden:

  * Ein CAS-Client ist jede CAS-fähige Anwendung, die über ein unterstütztes Protokoll mit dem Server kommunizieren kann.
  * Ein CAS-Client ist auch ein Softwarepaket, das in verschiedene Softwareplattformen und Anwendungen integriert werden kann, um mit dem CAS-Server über ein Authentifizierungsprotokoll (z.B. CAS, SAML, OAuth) zu kommunizieren. 

* Was ist die Aufgabe eines Tickets in einem TGT Service?

  Das TGT wird benötigt um Service Tickets (ST) zu generieren. Diese werden genutzt um einen User innerhalb eines individuellen CAS-gestützten Services durch sein TGT zu identifizieren und anzumelden.

## Quellen

* [CAS SSO With Spring Security | 05.11.2019]( https://www.baeldung.com/spring-security-cas-sso )
* [CAS Template | 05.11.2019]( https://github.com/apereo/cas-overlay-template )
* [CAS Configuration | 05.11.2019](https://apereo.github.io/cas/6.1.x/configuration/Configuration-Server-Management.html)
* [CAS Accept User | 05.11.2019](https://apereo.github.io/cas/6.1.x/configuration/Configuration-Properties.html#accept-users-authentication)
* [Spring Security CAS Authentication | 11.11.2019]( https://docs.spring.io/spring-security/site/docs/5.1.3.RELEASE/reference/html/servlet-webclient.html#cas )
* [Single Sign On | 13.11.2019](https://en.wikipedia.org/wiki/Single_sign-on)
* [CAS Protokoll |13.11.2019](https://apereo.github.io/cas/4.2.x/protocol/CAS-Protocol.html)